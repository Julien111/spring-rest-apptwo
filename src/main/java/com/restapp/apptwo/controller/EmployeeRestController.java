package com.restapp.apptwo.controller;

import com.restapp.apptwo.dto.EmployeeDto;
import com.restapp.apptwo.model.Employee;
import com.restapp.apptwo.service.EmployeeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class EmployeeRestController {

    private EmployeeService employeeService;

    public EmployeeRestController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/employees")
    public String getAllEmployee(Model model){
        List<EmployeeDto> employees = employeeService.listEmployees();
        model.addAttribute("employees", employees);
        return "list-employees";
    }

    @GetMapping("/employees/showFormAdd")
    public String showFormAdd(Model model){
        EmployeeDto employeeDto = new EmployeeDto();
        model.addAttribute("employee", employeeDto);
        return "addForm";
    }

    @PostMapping("/employees/save")
    public String addEmployee(@ModelAttribute("employee") EmployeeDto theEmployee){
        System.out.println(theEmployee.getEmail());
        employeeService.saveEmployee(theEmployee);
        return "redirect:/employees";
    }

    @GetMapping("/employees/{employeeId}")
    public EmployeeDto getEmployee(@PathVariable Long employeeId){
        return employeeService.getEmployeeById(employeeId);
    }

    @GetMapping("update/edit/{id}")
    public String editStudent(@PathVariable("id") Long id, Model model) {
        EmployeeDto employee = employeeService.getEmployeeById(id);

        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setFirstName(employee.getFirstName());
        employeeDto.setLastName(employee.getLastName());
        employeeDto.setEmail(employee.getEmail());

        model.addAttribute("employee", employeeDto);
        model.addAttribute("employee_id", employee.getId());
        return "edit";
    }


    @PostMapping("/employees/edit/{employeeId}")
    public String updateEmployee(@PathVariable Long employeeId, @ModelAttribute("studentForm") EmployeeDto theEmployee){

        EmployeeDto employee = employeeService.getEmployeeById(employeeId);
        employee.setFirstName(theEmployee.getFirstName());
        employee.setLastName(theEmployee.getLastName());
        employee.setEmail(theEmployee.getEmail());
        //save
        employeeService.updateEmployee(employeeId, theEmployee);

        return "redirect:/employees";
    }

    @DeleteMapping("/employees/delete/{employeeId}")
    public boolean deleteEmployee(@PathVariable Long employeeId){
        return employeeService.deleteEmployeeById(employeeId);
    }
}
