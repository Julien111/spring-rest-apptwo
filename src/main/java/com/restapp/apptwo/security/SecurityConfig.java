package com.restapp.apptwo.security;

import org.springframework.context.annotation.Configuration;

@Configuration
public class SecurityConfig {

//    use JDBC

//    @Bean
//    public UserDetailsManager userDetailsManager(DataSource dataSource){
//        return new JdbcUserDetailsManager(dataSource);
//    }
//
//    // 'fun123' and 'test123'
//
//    @Bean
//    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
//        http.authorizeHttpRequests(configurer ->
//                configurer
//                        .requestMatchers(HttpMethod.GET, "/api/employees").hasRole("EMPLOYEE")
//                        .requestMatchers(HttpMethod.GET, "/api/employees/**").hasRole("EMPLOYEE")
//                        .requestMatchers(HttpMethod.POST, "/api/employees/add").hasRole("MANAGER")
//                        .requestMatchers(HttpMethod.PUT, "/api/employees/edit/**").hasRole("MANAGER")
//                        .requestMatchers(HttpMethod.DELETE, "/api/employees/delete/**").hasRole("ADMIN"));
//        //use HTTP Basic authentication
//        http.httpBasic();
//        //disable CSRF request forgery
//        //not required for the REST API that use POST, PUT, DELETE and PATCH ...
//        http.csrf().disable();
//        return http.build();
//    }
}


//    @Bean
//    public InMemoryUserDetailsManager userDetailsManager() {
//
//        UserDetails john = User.builder()
//                .username("john")
//                .password("{noop}test123")
//                .roles("EMPLOYEE")
//                .build();
//
//        UserDetails jules = User.builder()
//                .username("jules")
//                .password("{noop}test321")
//                .roles("EMPLOYEE", "MANAGER")
//                .build();
//
//        UserDetails susan = User.builder()
//                .username("susan")
//                .password("{noop}12345")
//                .roles("EMPLOYEE", "MANAGER", "ADMIN")
//                .build();
//
//        return new InMemoryUserDetailsManager(john, jules, susan);
//    }