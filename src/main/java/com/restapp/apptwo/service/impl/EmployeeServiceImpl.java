package com.restapp.apptwo.service.impl;

import com.restapp.apptwo.dao.EmployeeDao;
import com.restapp.apptwo.dto.EmployeeDto;
import com.restapp.apptwo.model.Employee;
import com.restapp.apptwo.service.EmployeeService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeDao employeeDao;

    public EmployeeServiceImpl(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    @Override
    public Employee saveEmployee(EmployeeDto employeeDto) {
        Employee employee = new Employee();
        employee.setEmail(employeeDto.getEmail());
        employee.setFirstName(employeeDto.getFirstName());
        employee.setLastName(employeeDto.getLastName());
        return this.employeeDao.save(employee);
    }

    @Override
    public void saveEmployee(Employee employee) {
        this.employeeDao.save(employee);
    }

    @Override
    public List<EmployeeDto> listEmployees() {
        List<Employee> listEmployees = this.employeeDao.findAll();
        List<EmployeeDto> listFinal = new ArrayList<>();
        //add employee
        for(Employee employee : listEmployees){
            EmployeeDto employeeDto = new EmployeeDto();
            employeeDto.setId(employee.getId());
            employeeDto.setEmail(employee.getEmail());
            employeeDto.setFirstName(employee.getFirstName());
            employeeDto.setLastName(employee.getLastName());
            listFinal.add(employeeDto);
        }
        return listFinal;
    }

    @Override
    public EmployeeDto getEmployeeById(Long id) {
        Employee employee = employeeDao.findById(id).orElse(null);
        //test if null
        if(employee == null){
            throw new RuntimeException("Employee is not found");
        }
        //initialize the dto
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(employee.getId());
        employeeDto.setEmail(employee.getEmail());
        employeeDto.setFirstName(employee.getFirstName());
        employeeDto.setLastName(employee.getLastName());
        return employeeDto;
    }

    @Override
    public Employee updateEmployee(Long id, EmployeeDto employeeDto) {
        Employee employee = employeeDao.findById(id).orElse(null);

        //test if null
        if(employee == null){
            throw new RuntimeException("Employee is not found");
        }

        if(employeeDto.getEmail() != null && !employeeDto.getEmail().isEmpty()){
            employee.setEmail(employeeDto.getEmail());
        }
        if(employeeDto.getFirstName() != null && !employeeDto.getFirstName().isEmpty()){
            employee.setFirstName(employeeDto.getFirstName());
        }
        if(employeeDto.getLastName() != null && !employeeDto.getLastName().isEmpty()){
            employee.setLastName(employeeDto.getLastName());
        }
        return this.employeeDao.save(employee);
    }

    @Override
    public boolean existsById(Long id) {
        return employeeDao.existsById(id);
    }

    @Override
    public boolean deleteEmployeeById(Long id) {
        if(existsById(id)){
            employeeDao.deleteById(id);
            return true;
        }
        else{
            return false;
        }
    }
}
