package com.restapp.apptwo.service;

import com.restapp.apptwo.dto.EmployeeDto;
import com.restapp.apptwo.model.Employee;

import java.util.List;

public interface EmployeeService {

    Employee saveEmployee(EmployeeDto employeeDto);

    void saveEmployee(Employee employee);

    public List<EmployeeDto> listEmployees();

    public EmployeeDto getEmployeeById(Long id);

    public Employee updateEmployee(Long id, EmployeeDto employeeDto);

    public boolean existsById(Long id);

    public boolean deleteEmployeeById(Long id);

}
