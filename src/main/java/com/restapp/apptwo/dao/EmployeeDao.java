package com.restapp.apptwo.dao;

import com.restapp.apptwo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeDao extends JpaRepository<Employee, Long> {

    List<Employee> findAllByOrderByLastNameAsc();
}
